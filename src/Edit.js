import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';

class Edit extends Component {

  constructor(props) {
    super(props);
    this.state = {firstname:'', lastname:'',phone:''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
   console.log(this.props);
  }

componentDidMount() {
  fetch('/api/v1/users/'+ this.props.match.params.id)
  .then(response => {
			return response.json();
		}).then(result => {
			console.log(result);
			this.setState({
				id:result.id,
				firstname:result.data.firstname,
				lastname:result.data.lastname,
        phone:result.data.phone
			});
		});
  }

  handleChange(event) {
   const state = this.state
   state[event.target.name] = event.target.value
   this.setState(state);
 }

 handleSubmit(event) {
	  event.preventDefault();
	  fetch('/api/v1/users/'+ this.props.match.params.id, {
			method: 'POST',
			body: JSON.stringify({
							id:this.state.id,
							firstname: this.state.firstname,
							lastname: this.state.lastname,
              phone:this.state.phone
			}),
			headers: {
							"Content-type": "application/json; charset=UTF-8"
			}
		}).then(response => {
				if(response.status === 200) {
					alert("Edited");
				}
			});
  }
  render() {

    return <div>
      <Container>
        <h2>Edit User</h2>
        <Form onSubmit={this.handleSubmit}>
        <FormGroup>
          <Label for="firstname">Firstname</Label>
          <Input type="text" name="firstname"
                 onChange={this.handleChange} value={this.state.firstname} />
        </FormGroup>
          <FormGroup>
            <Label for="lastname">Lastname</Label>
            <Input type="text" name="lastname"
                   onChange={this.handleChange} value={this.state.lastname} />
          </FormGroup>
          <FormGroup>
            <Label for="phone">Phone</Label>
            <Input type="text" name="phone"
                   onChange={this.handleChange} value={this.state.phone}/>
          </FormGroup>
          <FormGroup>
            <Button color="primary" type="submit" >Save</Button>{' '}
            <Button color="danger" tag={Link} to="">Cancel</Button>
          </FormGroup>
        </Form>
      </Container>
    </div>
  }
}
export default Edit;
