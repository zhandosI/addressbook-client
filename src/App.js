import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import { Link } from 'react-router-dom';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {users: [], isLoading: true};
    // this.remove = this.remove.bind(this);
  }

  componentDidMount() {
    this.setState({isLoading: true});

    fetch('/api/v1/users')
      .then(response => response.json())
      .then(data => this.setState({users: data.data, isLoading: false}));
  }

  async delete(id) {
    await fetch(`/api/v1/users/${id}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(() => {
      let updatedUsers = [...this.state.users].filter(i => i.id !== id);
      this.setState({users: updatedUsers});
    });
  }

  render() {
    const {users, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    const userList = users.map(user => {
      return <tr key={user.id}>
        <td style={{whiteSpace: 'nowrap'}}>{user.firstname}</td>
        <td style={{whiteSpace: 'nowrap'}}>{user.lastname}</td>
        <td style={{whiteSpace: 'nowrap'}}>{user.phone}</td>
        <td>
          <ButtonGroup>
            <Button size="sm" color="primary" tag={Link} to={`/edit/${user.id}`}>Edit</Button>
            <Button size="sm" color="danger" onClick={() => this.delete(user.id)}>Delete</Button>
          </ButtonGroup>
        </td>
      </tr>
    });

    return (
      <div>
        <Container fluid>
          <div className="float-right">
            <Button color="success"  tag={Link} to="/create">Add User</Button>
          </div>
          <h3>AddressBook List</h3>
          <Table className="mt-4">
            <thead>
            <tr>
              <th width="20%">Firstname</th>
              <th width="20%">Lastname</th>
              <th width="20%">Phone</th>
              <th width="10%">Actions</th>
            </tr>
            </thead>
            <tbody>
            {userList}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default App;
